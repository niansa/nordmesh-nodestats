#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

#include <jsoncpp/json/json.h>
#include <curlpp/cURLpp.hpp>
#include <curlpp/Easy.hpp>
#include <curlpp/Options.hpp>


Json::Value getmesh(std::string URL) {
    // Initialise variables
    JSONCPP_STRING errs;
    Json::Value root;
	Json::CharReaderBuilder builder;
	const std::unique_ptr<Json::CharReader> reader(builder.newCharReader());
    std::ostringstream buffer;
    // Read data from server
    buffer << curlpp::options::Url(URL);
    // Parse JSON
    reader->parse(buffer.str().c_str(), buffer.str().c_str() + buffer.str().length(), &root, &errs);
    // Clean up
    buffer.clear();
    // Return root object
    return root;
}

Json::Value json_array_find_str_by_str(Json::Value jsonarray, std::string sby, std::string sfor) {
    unsigned int arraylen = jsonarray.size();
    unsigned int iternum = 0;
    while (jsonarray[iternum][sby] != sfor) {
        if (iternum == arraylen) {
            return Json::Value {};
        }
        iternum++;
    }
    return jsonarray[iternum];
}

int main(int argc, char** argv) {
    if (argc == 1) {
        std::cerr << "Usage: " << argv[0] << " <hostname> [key]" << std::endl;
        return 1;
    }
    // Do curl request
    auto mesh = getmesh("https://mesh.freifunknord.de/data/v2/meshviewer.json");
    // Get node from list by hostname
    auto node = json_array_find_str_by_str(mesh["nodes"], "hostname", argv[1]);
    // Print data
    if (node == Json::Value {}) {
        std::cerr << "No such node" << std::endl;
    }
    if (argc > 2) {
        if (node[argv[2]] == Json::Value {}) {
            std::cerr << "No such key" << std::endl;
        }
        std::cout << node[argv[2]];
    } else {
        std::cout << node;
    }
    std::cout << std::endl;
}
